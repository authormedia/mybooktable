const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: './src/blocks/index.tsx',
  mode: 'development',
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.scss', '.sass'],
  },
  externals: {
    '@wordpress/api-fetch': 'wp.apiFetch',
    '@wordpress/blocks': 'wp.blocks',
    '@wordpress/components': 'wp.components',
    '@wordpress/compose': 'wp.compose',
    '@wordpress/data': 'wp.data',
    '@wordpress/element': 'wp.element',
    '@wordpress/editor': 'wp.editor',
    '@wordpress/i18n': 'wp.i18n',
    '@wordpress/url': 'wp.url',
    'lodash': 'lodash',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: 'ts-loader',
      },
      {
        test: /\.s[ac]ss$/,
        exclude: /node_modules/,
        use: [ { loader: MiniCssExtractPlugin.loader }, 'css-loader?url=false', 'sass-loader'],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '../css/blocks.css',
    }),
  ],
  output: {
    filename: 'blocks.js',
    path: path.resolve(__dirname, '../js'),
  },
};
