declare const wp: any;

declare module '*.scss' {
  const classes: { [key: string]: string };
  export default classes;
}

declare module '*.jpg' {
  export default '' as string;
}

declare module '@wordpress/i18n' {
  export function __(str: string, scope?: string): string;
}

declare module '@wordpress/blocks' {
  export function createBlock(...args: any[]): any;
  export function registerBlockType(...args: any[]): any;
}

declare module '@wordpress/element' {
  export function RawHTML(...args: any[]): any;
  export class Component<P = {}, S = {}, SS = any> extends React.Component<P, S, SS> {};
  export class Fragment extends React.Component {};
}

declare module '@wordpress/components' {
  export class Button extends React.Component<any> {};
  export class Placeholder extends React.Component<any> {};
  export class PanelBody extends React.Component<any> {};
  export class Spinner extends React.Component<any> {};
  export class Toolbar extends React.Component<any> {};
  export class SelectControl extends React.Component<any> {};
  export class ToggleControl extends React.Component<any> {};
  export class RadioControl extends React.Component<any> {};
  export class TextareaControl extends React.Component<any> {};
  export class TextControl extends React.Component<any> {};
}

declare module '@wordpress/url' {
  export function addQueryArgs(...args: any[]): any;
}

declare module '@wordpress/api-fetch' {
  export default function apiFetch(...args: any[]): any;
}

declare module '@wordpress/editor' {
  export class BlockControls extends React.Component<any> {};
  export class InspectorControls extends React.Component<any> {};
}
