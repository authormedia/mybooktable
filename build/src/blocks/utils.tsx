/* tslint:disable-next-line no-any */
export interface AnyObject { [key: string]: any; }

export function assertNever(value: never): never {
  throw new Error(`Unexpected value ${value}`);
}

export function capitalize(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}
