import _ from 'lodash';
import { __ } from '@wordpress/i18n';
import { registerBlockType } from '@wordpress/blocks';
import { BlockControls, InspectorControls } from '@wordpress/editor';
import { RawHTML, Component, Fragment } from '@wordpress/element';
import {
  Button,
  Placeholder,
  PanelBody,
  Spinner,
  Toolbar,
  SelectControl,
  ToggleControl,
  RadioControl,
  TextareaControl,
  TextControl,
} from '@wordpress/components';
import { assertNever, AnyObject } from './utils';
import './index.scss';

interface CommonAttributeOptions {
  name: string;
  title: string;
  description?: string;
}
type AttributeOptions<Attributes extends AnyObject, Data> = CommonAttributeOptions & ( {
  type: 'dropdown';
  choices: Array<{ label: string; value: string }> | ((attributes: Attributes, data: Data | null) => Array<{ label: string; value: string }>);
  noEmptyOption?: boolean;
  default?: string;
} | {
  type: 'checkbox';
  default?: boolean;
} | {
  type: 'radio';
  choices: Array<{ label: string; value: string }>;
  default?: string;
} | {
  type: 'textarea';
  default?: string;
} | {
  type: 'text';
  default?: string;
} | {
  type: 'number';
  default?: string;
});
interface AttributeSection<Attributes extends AnyObject, Data> { name: string; attributes: Array<AttributeOptions<Attributes, Data>>; }
interface RegisterShortcodeBlockOptions<Attributes extends AnyObject, Data> {
  name: string;
  title: string;
  icon: string;
  description: string;
  data?: Promise<Data>;
  preview?: (attributes: Attributes, data: Data | null, edit: () => void) => React.ReactNode;
  inspectorAttributes?: Array<AttributeSection<Attributes, Data>>;
  attributes?: Array<AttributeOptions<Attributes, Data>>;
  toShortcode: (attributes: Attributes) => string;
}
interface RegisterShortcodeBlockProps<Attributes extends AnyObject, Data> { attributes: Attributes & { editMode: boolean }; setAttributes: ( attributes: Partial<Attributes> | { editMode?: boolean }) => void; }
type ShortcodeBlockEditorProps<Attributes extends AnyObject, Data> = RegisterShortcodeBlockProps<Attributes, Data> & { options: RegisterShortcodeBlockOptions<Attributes, Data> };
interface ShortcodeBlockEditorState<Attributes extends AnyObject, Data> { loading: boolean; data: Data | null; }

class ShortcodeBlockEditor<Attributes extends AnyObject, Data> extends Component<ShortcodeBlockEditorProps<Attributes, Data>, ShortcodeBlockEditorState<Attributes, Data>> {
  constructor(props: ShortcodeBlockEditorProps<Attributes, Data>) {
    super(props);

    this.state = {
      loading: false,
      data: null,
    };

    this.toggleEdit = this.toggleEdit.bind(this);
    this.attributeChange = _.memoize(this.attributeChange.bind(this));
    this.renderAttribute = this.renderAttribute.bind(this);
  }

  componentDidMount() {
    if(this.props.options.data) {
      this.setState({ loading: true });
      this.props.options.data.then(data =>this.setState({ data, loading: false }));
    }
  }

  toggleEdit() {
    this.props.setAttributes({ editMode: !this.props.attributes.editMode });
  }

  attributeChange(attribute: AttributeOptions<Attributes, Data>) {
    if(attribute.type === 'checkbox') {
      return () => this.props.setAttributes({[attribute.name]: !this.props.attributes[attribute.name]});
    } else if(attribute.type === 'number') {
      return (value: string) => {
        const numberValue = Math.floor(Number(value));
        return this.props.setAttributes({[attribute.name]: numberValue <= 0 ? '' : numberValue.toString()});
      };
    } else {
      return (value: string) => this.props.setAttributes({ [attribute.name]: value });
    }
  }

  renderAttribute(attribute: AttributeOptions<Attributes, Data>): React.ReactNode {
    const { attributes } = this.props;
    const { data } = this.state;

    if(attribute.type === 'dropdown') {
      const choices = _.isFunction(attribute.choices) ? attribute.choices(attributes, data) : attribute.choices;
      return <SelectControl
        key={attribute.name}
        label={attribute.title}
        help={attribute.description}
        value={attributes[attribute.name]}
        options={(attribute.noEmptyOption ? [] : [{ label: __('-- Choose One --', 'mybooktable'), value: '' }]).concat(choices)}
        onChange={this.attributeChange(attribute)}
      />;
    } else if(attribute.type === 'checkbox') {
      return <ToggleControl
        key={attribute.name}
        label={attribute.title}
        help={attribute.description}
        checked={attributes[attribute.name]}
        onChange={this.attributeChange(attribute)}
      />;
    } else if(attribute.type === 'radio') {
      return <RadioControl
        key={attribute.name}
        label={attribute.title}
        help={attribute.description}
        value={attributes[attribute.name]}
        options={attribute.choices}
        onChange={this.attributeChange(attribute)}
      />;
    } else if(attribute.type === 'textarea') {
      return <TextareaControl
        key={attribute.name}
        label={attribute.title}
        help={attribute.description}
        value={attributes[attribute.name]}
        onChange={this.attributeChange(attribute)}
      />;
    } else if(attribute.type === 'text') {
      return <TextControl
        key={attribute.name}
        label={attribute.title}
        help={attribute.description}
        value={attributes[attribute.name]}
        onChange={this.attributeChange(attribute)}
      />;
    } else if(attribute.type === 'number') {
      return <TextControl
        key={attribute.name}
        label={attribute.title}
        help={attribute.description}
        type="number"
        value={attributes[attribute.name]}
        onChange={this.attributeChange(attribute)}
      />;
    }

    return assertNever(attribute);
  }

  render() {
    const { attributes, options } = this.props;
    const { loading, data } = this.state;
    return <Fragment>
      {options.attributes && <BlockControls>
        <Toolbar controls={[{icon: 'edit', title: __('Edit'), onClick: this.toggleEdit, isActive: attributes.editMode}]} />
      </BlockControls>}
      {options.inspectorAttributes && <InspectorControls>
          {options.inspectorAttributes.map(section => <PanelBody title={section.name} key={section.name}>
              {section.attributes.map(this.renderAttribute)}
            </PanelBody>)}
        </InspectorControls>}
      <div className={`mbt-shortcode-block-editor ${options.name}`}>
        <Placeholder>
          <div className="header">
            <div className="logo" />
            <div>
              <h2>{__('MyBookTable', 'mybooktable')}</h2>
              <h1>{options.title}</h1>
            </div>
          </div>
          {attributes.editMode && options.attributes && <Fragment>
              <div className="description">{options.description}</div>
              <div className="attributes">
                {loading ? <div className="spinner-holder"><Spinner /></div> : options.attributes.map(this.renderAttribute)}
              </div>
              <Button isDefault onClick={this.toggleEdit}>{__('Done', 'mybooktable')}</Button>
            </Fragment>}
          {!attributes.editMode && options.preview && <div className="preview">{options.preview(attributes, data, this.toggleEdit)}</div>}
        </Placeholder>
      </div>
    </Fragment>;
  }
}

function getAttributeDataType<Attributes, Data>(attribute: AttributeOptions<Attributes, Data>): string {
  if(attribute.type === 'dropdown') {
    return 'string';
  } else if(attribute.type === 'checkbox') {
    return 'boolean';
  } else if(attribute.type === 'radio') {
    return 'string';
  } else if(attribute.type === 'textarea') {
    return 'string';
  } else if(attribute.type === 'text') {
    return 'string';
  } else if(attribute.type === 'number') {
    return 'string';
  }
  return assertNever(attribute);
}

export function registerShortcodeBlock<Attributes = {}, Data = null>(options: RegisterShortcodeBlockOptions<Attributes, Data>) {
  const attributes = _.flatten((options.inspectorAttributes || []).map(section => section.attributes)).concat(options.attributes || []);
  registerBlockType('mybooktable/' + options.name, {
    title: options.title,
    icon: options.icon,
    category: 'mybooktable',
    keywords: [__('MyBookTable', 'mybooktable')],
    description: options.description,
    attributes: _.assign(
      _.fromPairs(attributes.map(attribute => [attribute.name, { type: getAttributeDataType(attribute), default: attribute.default }])),
      { editMode: { type: 'boolean', default: true } }
    ),
    edit: (props: RegisterShortcodeBlockProps<Attributes, Data>) => <ShortcodeBlockEditor attributes={props.attributes} setAttributes={props.setAttributes} options={options} />,
    save: (props: RegisterShortcodeBlockProps<Attributes, Data>) => <RawHTML>{options.toShortcode(props.attributes)}</RawHTML>,
  });
}
