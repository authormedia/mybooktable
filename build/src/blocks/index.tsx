import _ from 'lodash';
import { __ } from '@wordpress/i18n';
import { addQueryArgs } from '@wordpress/url';
import apiFetch from '@wordpress/api-fetch';
import { capitalize } from './utils';
import { registerShortcodeBlock } from './shortcodes';

interface AllBooksAttributes {
  gridview: boolean;
  show_header: boolean;
  max_books: number;
}
registerShortcodeBlock<AllBooksAttributes>({
  name: 'all-books',
  title: __('All Books', 'mybooktable'),
  icon: 'book-alt',
  description: __('List all your books in an embedded book listing.', 'mybooktable'),
  inspectorAttributes: [
    {
      name: __('Display Settings', 'mybooktable'),
      attributes: [
        {
          name: 'gridview',
          type: 'checkbox',
          title: __('Force Grid View Display?', 'mybooktable'),
          description: __('Shows book covers in a responsive grid. (requires Professional or Developer Upgrade)', 'mybooktable'),
          default: false,
        },
        {
          name: 'show_header',
          type: 'checkbox',
          title: __('Show Header?', 'mybooktable'),
          description: __('Shows the taxonomy title and description above the book listing', 'mybooktable'),
          default: true,
        },
        {
          name: 'max_books',
          type: 'number',
          title: __('Max Books to Display', 'mybooktable'),
          description: __('Sets the maximum number of books that will be shown (Leave blank for no maximum)', 'mybooktable'),
          default: '',
        },
      ],
    },
  ],
  toShortcode: attributes => {
    const header = attributes.show_header ? '' : ' header="hidden"';
    const gridview = attributes.gridview ? ' gridview="true"' : '';
    const maxBooks = attributes.max_books ? ` max_books="${attributes.max_books}"` : '';
    return `[mybooktable${header}${gridview}${maxBooks}]`;
  },
});

interface RegisterrTaxonomyShortcodeBlockOptions<Taxonomy> {
  taxonomy: Taxonomy;
  title: string;
  description: string;
  singular: string;
  icon: string;
}
function registerTaxonomyShortcodeBlock<Taxonomy extends string, Attributes extends { [key in Taxonomy]: string }>(options: RegisterrTaxonomyShortcodeBlockOptions<Taxonomy>) {
  type TaxonomyAttributes = Attributes & {
    gridview: boolean;
    show_header: boolean;
    max_books: number;
  };
  type TaxonomyData = Array<{ name: string; slug: string }>;
  registerShortcodeBlock<TaxonomyAttributes, TaxonomyData>({
    name: `book-${options.taxonomy}`,
    title: options.title,
    icon: options.icon,
    description: options.description,
    data: apiFetch({ path: addQueryArgs(`/wp/v2/mbt_${options.taxonomy}`, { per_page: -1 }) }),
    preview: (attributes, data, edit) => {
      const term = data ? _.find(data, { slug: attributes[options.taxonomy] as string }) : null;
      return <div className="attribute">
        <span className="name">{options.singular}</span>
        {term ? <span className="value" onClick={edit}>{term.name}</span> : <span className="no-value">{__('None', 'mybooktable')}</span>}
      </div>;
    },
    attributes: [
      {
        name: options.taxonomy,
        type: 'dropdown',
        title: options.singular,
        choices: (attributes, data) => data ? data.map(term => ({ label: term.name, value: term.slug })) : [],
        default: '',
      },
    ],
    inspectorAttributes: [
      {
        name: __('Display Settings', 'mybooktable'),
        attributes: [
          {
            name: 'gridview',
            type: 'checkbox',
            title: __('Force Grid View Display?', 'mybooktable'),
            description: __('Shows book covers in a responsive grid. (requires Professional or Developer Upgrade)', 'mybooktable'),
            default: false,
          },
          {
            name: 'show_header',
            type: 'checkbox',
            title: __('Show Header?', 'mybooktable'),
            description: __('Shows the taxonomy title and description above the book listing', 'mybooktable'),
            default: true,
          },
          {
            name: 'max_books',
            type: 'number',
            title: __('Max Books to Display', 'mybooktable'),
            description: __('Sets the maximum number of books that will be shown (Leave blank for no maximum)', 'mybooktable'),
            default: '',
          },
        ],
      },
    ],
    toShortcode: attributes => {
      if(!attributes[options.taxonomy]) { return ''; }
      const header = attributes.show_header ? '' : ' header="hidden"';
      const gridview = attributes.gridview ? ' gridview="true"' : '';
      const maxBooks = attributes.max_books ? ` max_books="${attributes.max_books}"` : '';
      return `[mybooktable ${options.taxonomy}="${attributes[options.taxonomy]}"${header}${gridview}${maxBooks}]`;
    },
  });
}

registerTaxonomyShortcodeBlock({
  taxonomy: 'series',
  title: __(`All Books in Series`, 'mybooktable'),
  description: __(`List all the books in a given series in an embedded book listing.`, 'mybooktable'),
  singular: __(`Series`, 'mybooktable'),
  icon: 'admin-page',
});
registerTaxonomyShortcodeBlock({
  taxonomy: 'genre',
  title: __(`All Books in Genre`, 'mybooktable'),
  description: __(`List all the books in a given genre in an embedded book listing.`, 'mybooktable'),
  singular: __(`Genre`, 'mybooktable'),
  icon: 'category',
});
registerTaxonomyShortcodeBlock({
  taxonomy: 'tag',
  title: __(`All Books with Tag`, 'mybooktable'),
  description: __(`List all the books with a given tag in an embedded book listing.`, 'mybooktable'),
  singular: __(`Tag`, 'mybooktable'),
  icon: 'tag',
});
registerTaxonomyShortcodeBlock({
  taxonomy: 'author',
  title: __(`All Books by Author`, 'mybooktable'),
  description: __(`List all the books written by a given author in an embedded book listing.`, 'mybooktable'),
  singular: __(`Author`, 'mybooktable'),
  icon: 'admin-users',
});

interface SingleBookAttributes {
  book: string;
  display: string;
  buybutton_shadowbox: boolean;
}
type SingleBookData = Array<{ title: { rendered: string }; slug: string }>;
registerShortcodeBlock<SingleBookAttributes, SingleBookData>({
  name: 'single-book',
  title: __('Single Book', 'mybooktable'),
  icon: 'book-alt',
  description: __('Show a given book in an embedded book listing.', 'mybooktable'),
  data: apiFetch({ path: addQueryArgs('/wp/v2/mbt_book', { per_page: -1 }) }),
  preview: (attributes, data, edit) => {
    const book = data ? _.find(data, { slug: attributes.book as string }) : null;
    return <div className="attribute">
      <span className="name">{__('Book', 'mybooktable')}</span>
      {book ? <span className="value" onClick={edit}>{book.title.rendered}</span> : <span className="no-value">{__('None', 'mybooktable')}</span>}
    </div>;
  },
  attributes: [
    {
      name: 'book',
      type: 'dropdown',
      title: __('Book', 'mybooktable'),
      choices: (attributes, data) => data ? data.map(book => ({ label: book.title.rendered, value: book.slug })) : [],
      default: '',
    },
  ],
  inspectorAttributes: [
    {
      name: __('Display Settings', 'mybooktable'),
      attributes: [
        {
          name: 'display',
          type: 'dropdown',
          title: __('Display Style', 'mybooktable'),
          choices: [
            { label: __('Default', 'mybooktable'), value: 'default' },
            { label: __('Summary', 'mybooktable'), value: 'summary' },
            { label: __('Buy Buttons', 'mybooktable'), value: 'buybuttons' },
            { label: __('Cover and Buy Buttons', 'mybooktable'), value: 'cover+buybuttons' },
          ],
          noEmptyOption: true,
          default: 'default',
        },
        {
          name: 'buybutton_shadowbox',
          type: 'checkbox',
          title: __('Force Shadow Box for Buy Buttons?', 'mybooktable'),
          default: false,
        },
      ],
    },
  ],
  toShortcode: attributes => {
    if(!attributes.book) { return ''; }
    const display = attributes.display !== 'default' ? ` display="${attributes.display}"` : '';
    const buybuttonShadowbox = attributes.buybutton_shadowbox ? ' buybutton_shadowbox="true"' : '';
    return `[mybooktable book="${attributes.book}"${display}${buybuttonShadowbox}]`;
  },
});

interface TermListAttributes {
  taxonomy: string;
  display: string;
}
registerShortcodeBlock<TermListAttributes>({
  name: 'term-list',
  title: __('All Terms in Taxonomy', 'mybooktable'),
  icon: 'list-view',
  description: __('This allows you to display all of the different items in a MyBookTable taxonomy.', 'mybooktable'),
  preview: (attributes, data, edit) => {
    return <div className="attribute">
      <span className="name">{__('Taxonomy', 'mybooktable')}</span>
      {attributes.taxonomy ? <span className="value" onClick={edit}>{capitalize(attributes.taxonomy)}</span> : <span className="no-value">{__('None', 'mybooktable')}</span>}
    </div>;
  },
  attributes: [
    {
      name: 'taxonomy',
      type: 'dropdown',
      title: __('Taxonomy', 'mybooktable'),
      choices: [
        { label: __('Series', 'mybooktable'), value: 'series' },
        { label: __('Genres', 'mybooktable'), value: 'genres' },
        { label: __('Tags', 'mybooktable'), value: 'tags' },
        { label: __('Authors', 'mybooktable'), value: 'authors' },
      ],
      default: '',
    },
  ],
  inspectorAttributes: [
    {
      name: __('Display Settings', 'mybooktable'),
      attributes: [
        {
          name: 'display',
          type: 'dropdown',
          title: __('Display Style', 'mybooktable'),
          choices: [
            { label: __('Listing', 'mybooktable'), value: 'listing' },
            { label: __('Menu Bar', 'mybooktable'), value: 'bar' },
            { label: __('Simple', 'mybooktable'), value: 'simple' },
          ],
          noEmptyOption: true,
          default: 'listing',
        },
      ],
    },
  ],
  toShortcode: attributes => {
    if(!attributes.taxonomy) { return ''; }
    return `[mybooktable list="${attributes.taxonomy}" display="${attributes.display}"]`;
  },
});
